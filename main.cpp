#include <iostream>
#include <cstdlib>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;


void header();
int main(int argc, char ** argv) {

   // Criação de objetos com alocação estática
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);

   // Criação de objetos com alocação dinâmica
   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);

   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);

   // Impressão dos atributos dos objetos utilizando método da própria classe
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();

   // Criação de objeto da classe filha
   Aluno aluno_1;
   aluno_1.setNome("João");
   cout << "Curso do aluno " << aluno_1.getNome() << ": " << aluno_1.getCurso() << endl;

   // Exemplo de cadastro de uma lista de Alunos
   Aluno *lista_de_alunos[20]; // Criação da lista de tamanho fixo de ponteiros do objeto Aluno

   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
   float ira;
   int semestre;
   string curso;
   int n;
   string sala;
   string formacao;

   // Entrada de dados do terminal (stdin)
   cout << "Nome: ";
   cin >> nome;
   cout << "Matricula: ";
   cin >> matricula;
   cout << "Idade: ";
   cin >> idade;

   // Criação do primeiro objeto da lista
   lista_de_alunos[0] = new Aluno();

   // Definição dos atributos do objeto
   lista_de_alunos[0]->setNome(nome);
   lista_de_alunos[0]->setMatricula(matricula);
   lista_de_alunos[0]->setIdade(idade);

   // Impressão dos atributos do objeto
   lista_de_alunos[0]->imprimeDadosAluno();

   // Liberação de memória dos objetos criados dinamicamente
   delete(pessoa_3);
   delete(pessoa_4);

   header();

   Professor *lista_de_professores[10];
   std::cout << "Informe quantos professores deseja cadastrar: " << '\n';
   std::cin >> n;
   for(int i=1; i<=n; ++i){

       while(n>=10){
           std::cout << "\nNumero maximo de cadastro de professores: 10" << '\n';
           std::cout << "Informe quantos professores deseja cadastrar: " << '\n';
           std::cin >> n;
       }
       cout << "\nNome[" <<i<<"]: "<< endl;
       cin >> nome;
       cout << "\nFormacao[" <<i<<"]: "<< endl;
       cin >> formacao;
       cout << "\nSala[" <<i<<"]: " << endl;
       cin >> sala;

       lista_de_professores[i] = new Professor();

       lista_de_professores[i]->setNome(nome);
       lista_de_professores[i]->setFormacao(formacao);
       lista_de_professores[i]->setSala(sala);

       // Impressão dos atributos do objeto
       system("clear");
       if(i==n){
           for(int j=1; j<=n; ++j){
               lista_de_professores[j]->imprimeDadosProfessor();
           }
       }

   }



   return 0;
}

void header(){
    system("clear");
    std::cout << "##################################################" << '\n';
    std::cout << "\t\tPROFESSORES" << '\n';
}
