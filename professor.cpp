#include "professor.hpp"
#include <iostream>

using namespace std;

Professor::Professor(){
    setNome("Renato Coral");
    setSexo("Masculino");
    setFormacao("Engenharia de Software");
    setSala("I7");
}

Professor::Professor(string nome, string formacao, string sala){
    setNome(nome);
    setFormacao(formacao);
    setSala(sala);
}

Professor::~Professor(){}

string Professor::getFormacao(){
    return formacao;
}

void Professor::setFormacao(string formacao){
    this->formacao=formacao;

}
float Professor::getSalario(){
    return salario;
}

void Professor::setSalario(float salario){
    this->salario=salario;
}

string Professor::getSala(){
    return sala;
}

void Professor::setSala(string sala){
    this-> sala= sala;
}

void Professor::imprimeDadosProfessor(){
    std::cout << "##################################################" << '\n';
    std::cout << "\t\tPROFESSORES" << '\n';
    cout << "Dados do Professor" << endl;
    cout << "Nome: " << getNome() << endl;
    cout << "Formacao: " << getFormacao() << endl;
    cout << "Sala: " << getSala() << endl;
}
