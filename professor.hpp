#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP
#include <iostream>
#include <string>
#include "pessoa.hpp"

class Professor: public Pessoa{
private: //atributos privados
    string formacao;
    float salario;
    string sala;

public: //construtores e metodos acessores publicos
    Professor();
    Professor(string nome, string formacao, string sala);
    ~Professor();

    string getFormacao();
    void setFormacao(string formacao);
    float getSalario();
    void setSalario(float salario);
    string getSala();
    void setSala(string sala);

    void imprimeDadosProfessor();

};
#endif
